package fr.cnam.foad.nfa035.badges.gui;

import com.github.lgooddatepicker.components.DatePicker;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.jdesktop.swingx.JXDatePicker;

import javax.swing.*;
import javax.xml.stream.Location;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

public class AddBadgeDialog extends JDialog implements PropertyChangeListener {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField codeSerie;
    private JFileChooser fileChooser;
    private JXDatePicker dateFin;
    private JXDatePicker dateDebut;
    BadgeWalletGUI caller;
    DigitalBadge badge;
    DirectAccessBadgeWalletDAO dao;

    public void setCaller(BadgeWalletGUI caller) {
        this.caller = caller;
    }

    public void setDao(DirectAccessBadgeWalletDAO dao) {
        this.dao = dao;
    }

    /**
     * Commentez-moi
     *
     * @return ImageIcon
     */
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/logo.png"));
    }

    public AddBadgeDialog() {

        this.caller = caller;
        fileChooser = new JFileChooser();
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


    }

    /**
     *
     */
    private void onOK() {

        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        AddBadgeDialog dialog = new AddBadgeDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    /**
     * Cette méthode est appelée lorsqu'une propriété liée est modifiée
     *
     * @param evt A PropertyChangeEvent object describing the event source
     *            and the property that has changed.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (validateForm()) {
            this.buttonOK.setEnabled(true);
        } else {
            this.buttonOK.setEnabled(false);
        }

    }

    private boolean validateForm() {
    if(codeSerie.isValid() && dateDebut.isValid() && dateFin.isValid())
         return true;
    else
        return false;
    }



}
